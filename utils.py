import nltk
import pandas as pd
from decouple import config
from nltk.corpus import stopwords

FILENAME = config('FILENAME', cast=str, default='labeled_data.csv')


def init_work():
    nltk.download('averaged_perceptron_tagger')
    nltk.download('stopwords')


def get_dataset():
    return pd.read_csv(f'datasets/{FILENAME}')


def get_stopwords():
    words = stopwords.words('english')
    other_exclusions = ["#ff", "ff", "rt"]

    words.extend(other_exclusions)

    return words
