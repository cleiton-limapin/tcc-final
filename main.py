import os
import warnings

import numpy as np
from decouple import config
from nltk.cluster import cosine_distance
from sklearn.feature_selection import SelectFromModel
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import StratifiedKFold, GridSearchCV, \
    train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.pipeline import Pipeline

from hatespeech import HateSpeechClassifier
from utils import init_work

warnings.simplefilter(action='ignore', category=FutureWarning)

RUN_TIMES = config('RUN_TIMES', default=1, cast=int)
N_JOBS = config('N_JOBS', default=1, cast=int)
N_NEIGHBORS = config('N_NEIGHBORS', default=30, cast=int)
SAVE_DATA = config('SAVE_DATA', default=False, cast=bool)
USE_KNN_FILE = config('USE_KNN_FILE', default=False, cast=bool)

USE_KNN = config('USE_KNN', default=True, cast=bool)
USE_META_FEATURES = config('USE_META_FEATURES', default=True, cast=bool)
USE_ORIGINAL = config('USE_ORIGINAL', default=True, cast=bool)


FILEPATH= 'results_knn.csv'


def get_labels_and_indices(labels, ind):
    l0 = []
    l1 = []
    l2 = []
    for i in ind:
        if labels[i] == 0:
            l0.append(i)
        elif labels[i] == 1:
            l1.append(i)
        elif labels[i] == 2:
            l2.append(i)

    return (l0, 0), (l1, 1), (l2, 2)


def build_meta_features(neigh, X, y):
    inds = neigh.kneighbors(return_distance=False)
    kneighbors_size = np.size(inds, axis=0)

    def count_labels(a, lbs, values):
        lc = lbs[a]

        return [np.size(lc[lc == x]) for x in values]

    def count_max_labels(a, lbs, values):
        lc = lbs[a]
        count = [np.size(lc[lc == x]) for x in values]
        big = np.max(count)

        return [x/big for x in values]

    label_values = (0, 1, 2)
    counts_label = np.apply_along_axis(
        count_labels, 1, inds, y, label_values)

    counts_max_label = np.apply_along_axis(
        count_max_labels, 1, inds, y, label_values)

    splitter = round(N_NEIGHBORS * 0.25)

    m = np.zeros((kneighbors_size, 15))

    for i, ind in enumerate(inds):
        xi = X[i]

        for indexes, label in get_labels_and_indices(y, ind):
            if not indexes:
                continue

            min_dist = indexes[0]
            max_dist = indexes[-1]
            mean_dist = indexes[round(len(indexes) / 2)]
            qinf_dist = indexes[:splitter][-1] if len(indexes) > splitter else indexes[0]
            qsup_dist = indexes[splitter:][0] if len(indexes) > splitter else indexes[0]

            mid = X[min_dist]
            mad = X[max_dist]
            med = X[mean_dist]
            qinf = X[qinf_dist]
            qsup = X[qsup_dist]

            col = label * 5

            m[i][col] = cosine_distance(xi, mid)
            m[i][col+1] = cosine_distance(xi, mad)
            m[i][col+2] = cosine_distance(xi, med)
            m[i][col+3] = cosine_distance(xi, qinf)
            m[i][col+4] = cosine_distance(xi, qsup)

        print(f'Terminando features... {i}\r', end='', flush=True)

    print()
    X = np.concatenate((X, counts_label, counts_max_label, m), axis=1)

    return X, y


def build_meta_features_complete(neigh, X, y):
    inds = neigh.kneighbors(return_distance=False)
    kneighbors_size = np.size(inds, axis=0)

    def count_labels(a, lbs, values):
        lc = lbs[a]

        return [np.size(lc[lc == x]) for x in values]

    def count_max_labels(a, lbs, values):
        lc = lbs[a]
        count = [np.size(lc[lc == x]) for x in values]
        big = np.max(count)

        return [x/big for x in values]

    label_values = (0, 1, 2)
    counts_label = np.apply_along_axis(
        count_labels, 1, inds, y, label_values)

    counts_max_label = np.apply_along_axis(
        count_max_labels, 1, inds, y, label_values)

    m = np.zeros((kneighbors_size, 30))

    for i, ind in enumerate(inds):
        xi = X[i]

        for index, x in enumerate(ind):
            m[i][index] = cosine_distance(xi, X[x])

    X = np.concatenate((X, counts_label, counts_max_label, m), axis=1)

    return X, y


def execute_original(return_feats_names=False):
    csf = HateSpeechClassifier()
    X, y = csf.build()

    if return_feats_names:
        return X, y, csf.feature_names

    return X, y


def execute_knn(X, y):
    print('Running KNN...')

    if USE_KNN_FILE and os.path.exists(FILEPATH):
        print('Reading "{0}" to load KNN...'.format(FILEPATH))
        data = np.genfromtxt(FILEPATH, delimiter=',')

        return data

    neigh = KNeighborsClassifier(n_neighbors=N_NEIGHBORS, n_jobs=N_JOBS)
    neigh.fit(X, y)

    return neigh


def run_meta_features_only():
    for n in range(1, RUN_TIMES + 1):
        print('Execução {}/{}'.format(n, RUN_TIMES), end='\n\n')

        X, y, feats_names = execute_original(return_feats_names=True)
        neigh = execute_knn(X, y)
        X_meta, y_meta = build_meta_features(neigh, X, y)

        X_meta = X_meta[:, X_meta.shape[1]-21:]

        print('Split dataset with test and train...')
        X_train, X_test, y_train, y_test = train_test_split(
            X_meta, y_meta, random_state=None, test_size=0.1
        )

        select_model = SelectFromModel(
            LogisticRegression(class_weight='balanced', penalty='l1', C=0.01)
        )
        LR = LogisticRegression(class_weight='balanced', penalty='l2')

        pipeline = Pipeline(steps=[
            ('select', select_model),
            ('model', LR)
        ])

        param_grid = [{}]

        print('Running GridSearchCV...')
        grid_search = GridSearchCV(
            estimator=pipeline,
            param_grid=param_grid,
            cv=StratifiedKFold(n_splits=5, random_state=42).split(X_train, y_train),
            verbose=1,
            n_jobs=N_JOBS
        )

        model = grid_search.fit(X_train, y_train)
        y_preds = model.predict(X_test)

        conf_matrix = confusion_matrix(y_test, y_preds)

        matrix_proportions = np.zeros((3, 3))
        for i in range(0, 3):
            matrix_proportions[i, :] = conf_matrix[i, :] / float(
                conf_matrix[i, :].sum())

        with open(f'result-meta-only-{n}.txt', 'w') as f:
            f0, f1, f2 = (
                matrix_proportions[0][0],
                matrix_proportions[1][1],
                matrix_proportions[2][2]
            )
            f.write('{}, {}, {}\n'.format(f0, f1, f2))

        print(matrix_proportions)


def run_origial():
    for n in range(1, RUN_TIMES + 1):
        print('Execução {}/{}'.format(n, RUN_TIMES), end='\n\n')
        X, y, feat_names = execute_original(return_feats_names=True)

        print('Split dataset with test and train...')
        X_train, X_test, y_train, y_test = train_test_split(
            X, y, random_state=None, test_size=0.1
        )

        select_model = SelectFromModel(
            LogisticRegression(class_weight='balanced', penalty='l1', C=0.01)
        )
        LR = LogisticRegression(class_weight='balanced', penalty='l2')

        pipeline = Pipeline(steps=[
            ('select', select_model),
            ('model', LR)
        ])

        param_grid = [{}]  # Optionally add parameters here

        print('Running GridSearchCV...')
        grid_search = GridSearchCV(
            estimator=pipeline,
            param_grid=param_grid,
            cv=StratifiedKFold(n_splits=5, random_state=42).split(X_train, y_train),
            verbose=1,
            n_jobs=N_JOBS
        )

        model = grid_search.fit(X_train, y_train)
        y_preds = model.predict(X_test)

        # Evaluating the results
        # report = classification_report(y_test, y_preds)

        conf_matrix = confusion_matrix(y_test, y_preds)

        matrix_proportions = np.zeros((3, 3))
        for i in range(0, 3):
            matrix_proportions[i, :] = conf_matrix[i, :] / float(conf_matrix[i, :].sum())

        with open(f'result-original-{n}.txt', 'w') as f:
            f0, f1, f2 = (
                matrix_proportions[0][0],
                matrix_proportions[1][1],
                matrix_proportions[2][2]
            )
            f.write('{}, {}, {}\n'.format(f0, f1, f2))

        print(matrix_proportions)


def run_original_with_meta_features():
    for n in range(1, RUN_TIMES + 1):
        print('Execução {}/{}'.format(n, RUN_TIMES), end='\n\n')
        X, y, feat_names = execute_original(return_feats_names=True)
        neigh = execute_knn(X, y)
        X, y = build_meta_features(neigh, X, y)

        print('Split dataset with test and train...')
        X_train, X_test, y_train, y_test = train_test_split(
            X, y, random_state=None, test_size=0.1
        )

        select_model = SelectFromModel(
            LogisticRegression(class_weight='balanced', penalty='l1', C=0.01)
        )
        LR = LogisticRegression(class_weight='balanced', penalty='l2')

        pipeline = Pipeline(steps=[
            ('select', select_model),
            ('model', LR)
        ])

        param_grid = [{}]  # Optionally add parameters here

        print('Running GridSearchCV...')
        grid_search = GridSearchCV(
            estimator=pipeline,
            param_grid=param_grid,
            cv=StratifiedKFold(n_splits=5, random_state=42).split(X_train, y_train),
            verbose=1,
            n_jobs=N_JOBS
        )

        model = grid_search.fit(X_train, y_train)
        y_preds = model.predict(X_test)

        # Evaluating the results
        # report = classification_report(y_test, y_preds)

        conf_matrix = confusion_matrix(y_test, y_preds)

        matrix_proportions = np.zeros((3, 3))
        for i in range(0, 3):
            matrix_proportions[i, :] = conf_matrix[i, :] / float(conf_matrix[i, :].sum())

        with open(f'result-original-meta-{n}.txt', 'w') as f:
            f0, f1, f2 = (
                matrix_proportions[0][0],
                matrix_proportions[1][1],
                matrix_proportions[2][2]
            )
            f.write('{}, {}, {}\n'.format(f0, f1, f2))

        print(matrix_proportions)


def run_original_with_meta_features_complete():
    for n in range(1, RUN_TIMES + 1):
        print('Execução {}/{}'.format(n, RUN_TIMES), end='\n\n')
        X, y, feat_names = execute_original(return_feats_names=True)
        neigh = execute_knn(X, y)
        X, y = build_meta_features_complete(neigh, X, y)

        print('Split dataset with test and train...')
        X_train, X_test, y_train, y_test = train_test_split(
            X, y, random_state=None, test_size=0.1
        )

        select_model = SelectFromModel(
            LogisticRegression(class_weight='balanced', penalty='l1', C=0.01)
        )
        LR = LogisticRegression(class_weight='balanced', penalty='l2')

        pipeline = Pipeline(steps=[
            ('select', select_model),
            ('model', LR)
        ])

        param_grid = [{}]  # Optionally add parameters here

        print('Running GridSearchCV...')
        grid_search = GridSearchCV(
            estimator=pipeline,
            param_grid=param_grid,
            cv=StratifiedKFold(n_splits=5, random_state=42).split(X_train, y_train),
            verbose=1,
            n_jobs=N_JOBS
        )

        model = grid_search.fit(X_train, y_train)
        y_preds = model.predict(X_test)

        # Evaluating the results
        # report = classification_report(y_test, y_preds)

        conf_matrix = confusion_matrix(y_test, y_preds)

        matrix_proportions = np.zeros((3, 3))
        for i in range(0, 3):
            matrix_proportions[i, :] = conf_matrix[i, :] / float(conf_matrix[i, :].sum())

        with open(f'result-original-meta-complete-{n}.txt', 'w') as f:
            f0, f1, f2 = (
                matrix_proportions[0][0],
                matrix_proportions[1][1],
                matrix_proportions[2][2]
            )
            f.write('{}, {}, {}\n'.format(f0, f1, f2))

        print(matrix_proportions)


def run_original_with_svm():
    for n in range(1, RUN_TIMES + 1):
        print('Execução {}/{}'.format(n, RUN_TIMES), end='\n\n')
        X, y, feat_names = execute_original(return_feats_names=True)

        print('Split dataset with test and train...')
        X_train, X_test, y_train, y_test = train_test_split(
            X, y, random_state=None, test_size=0.1
        )

        # select_model = SelectFromModel(
        #     SVC(class_weight='balanced', C=0.01)
        # )
        SVM = SVC(class_weight='balanced', C=0.01)

        pipeline = Pipeline(steps=[
            # ('select', select_model),
            ('model', SVM)
        ])

        param_grid = [{}]  # Optionally add parameters here

        print('Running GridSearchCV...')
        grid_search = GridSearchCV(
            estimator=pipeline,
            param_grid=param_grid,
            cv=StratifiedKFold(n_splits=5, random_state=42).split(X_train, y_train),
            verbose=1,
            n_jobs=N_JOBS
        )

        model = grid_search.fit(X_train, y_train)
        y_preds = model.predict(X_test)

        # Evaluating the results
        # report = classification_report(y_test, y_preds)

        conf_matrix = confusion_matrix(y_test, y_preds)

        matrix_proportions = np.zeros((3, 3))
        for i in range(0, 3):
            matrix_proportions[i, :] = conf_matrix[i, :] / float(conf_matrix[i, :].sum())

        with open(f'result-original-svm{n}.txt', 'w') as f:
            f0, f1, f2 = (
                matrix_proportions[0][0],
                matrix_proportions[1][1],
                matrix_proportions[2][2]
            )
            f.write('{}, {}, {}\n'.format(f0, f1, f2))

        print(matrix_proportions)


init_work()


# names = ['Hate', 'Offensive', 'Neither']
# confusion_df = pd.DataFrame(matrix_proportions, index=names, columns=names)
# plt.figure(figsize=(5, 5))
#
# seaborn.heatmap(
#     confusion_df,
#     annot=True,
#     annot_kws={'size': 12},
#     cmap='gist_gray_r',
#     cbar=False,
#     square=True,
#     fmt='.2f'
# )
# plt.ylabel('True categories', fontsize=14)
# plt.xlabel('Predicted categories', fontsize=14)
# plt.tick_params(labelsize=12)
# plt.show()
