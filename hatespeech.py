import re

import numpy as np
import pandas as pd
from nltk import pos_tag
from nltk.stem.snowball import PorterStemmer
from sklearn.feature_extraction.text import TfidfVectorizer
from textstat.textstat import textstat
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer

from utils import get_dataset, get_stopwords


class HateSpeechClassifier:
    """
    Classificador de discurso de ódio
    """

    def __init__(self):
        self.df = get_dataset()
        self.stopwords = get_stopwords()
        self.stemmer = PorterStemmer()

        self.vocab = None
        self.pos_vocab = None
        self.other_features_names = None
        self.feature_names = None

    def build_tfidf(self):
        vectorizer = TfidfVectorizer(
            tokenizer=self._tokenize,
            preprocessor=self._preprocess,
            ngram_range=(1, 3),
            stop_words=self.stopwords,
            use_idf=True,
            smooth_idf=False,
            norm=None,
            decode_error='replace',
            max_features=10000,
            min_df=5,
            max_df=0.75
        )
        tfidf = vectorizer.fit_transform(self.df.tweet).toarray()

        self.vocab = {
            v: i for i, v in enumerate(vectorizer.get_feature_names())
        }

        return tfidf

    def build_pos_tags(self):
        tweet_tags = []
        texts = self.df.tweet

        for text in texts:
            tokens = self._basic_tokenize(self._preprocess(text))
            tags = pos_tag(tokens)
            tag_list = [x[1] for x in tags]
            tag_str = ' '.join(tag_list)
            tweet_tags.append(tag_str)

        pos_vectorizer = TfidfVectorizer(
            tokenizer=None,
            lowercase=False,
            preprocessor=None,
            ngram_range=(1, 3),
            stop_words=None,
            use_idf=False,
            smooth_idf=False,
            norm=None,
            decode_error='replace',
            max_features=5000,
            min_df=5,
            max_df=0.75
        )

        pos = pos_vectorizer.fit_transform(pd.Series(tweet_tags)).toarray()

        self.pos_vocab = {v: i for i, v in enumerate(pos_vectorizer.get_feature_names())}

        return pos

    def build_others_feats(self):
        sentiment_analyzer = SentimentIntensityAnalyzer()

        def get_sentiment(tweet):
            """
            This function takes a string and returns a list of features.
            These include Sentiment scores, Text and Readability scores,
            as well as Twitter specific features
            """
            sentiment = sentiment_analyzer.polarity_scores(tweet)
            words = self._preprocess(tweet)  # Get text only

            syllables = textstat.syllable_count(words)
            num_chars = sum(len(w) for w in words)
            num_chars_total = len(tweet)
            num_terms = len(tweet.split())
            num_words = len(words.split())
            avg_syl = round(
                float((syllables + 0.001)) / float(num_words + 0.001),
                4)
            num_unique_terms = len(set(words.split()))

            # Modified FK grade, where avg words per sentence is just num
            # words/1
            FKRA = round(
                float(0.39 * float(num_words) / 1.0) + float(
                    11.8 * avg_syl) - 15.59, 1)
            # Modified FRE score, where sentence fixed to 1
            FRE = round(
                206.835 - 1.015 * (float(num_words) / 1.0) - (
                        84.6 * float(avg_syl)), 2)

            twitter_objs = self._count_twitter_objs(tweet)
            retweet = 0
            if 'rt' in words:
                retweet = 1
            features = [
                FKRA, FRE, syllables, avg_syl, num_chars, num_chars_total,
                num_terms, num_words,
                num_unique_terms, sentiment['neg'], sentiment['pos'],
                sentiment['neu'], sentiment['compound'],
                twitter_objs[2], twitter_objs[1],
                twitter_objs[0], retweet
            ]
            # features = pandas.DataFrame(features)

            return features

        result = []
        for t in self.df.tweet:
            result.append(get_sentiment(t))

        self.other_features_names = [
            'FKRA', 'FRE', 'num_syllables', 'avg_syl_per_word', 'num_chars',
            'num_chars_total', 'num_terms', 'num_words', 'num_unique_words',
            'vader neg', 'vader pos', 'vader neu', 'vader compound',
            'num_hashtags', 'num_mentions', 'num_urls', 'is_retweet'
        ]

        return np.array(result)

    def build(self):
        tfidf = self.build_tfidf()
        pos = self.build_pos_tags()
        feats = self.build_others_feats()

        x = np.concatenate([tfidf, pos, feats], axis=1)

        # x = pd.DataFrame(m)
        y = self.df['class'].astype(int).values

        # gera o nome das features
        variables = [''] * len(self.vocab)
        for k, v in self.vocab.items():
            variables[v] = k

        pos_variables = [''] * len(self.pos_vocab)
        for k, v in self.pos_vocab.items():
            pos_variables[v] = k

        self.feature_names = variables + pos_variables + self.other_features_names

        return x, y

    def _preprocess(self, text_string):
        """
        Accepts a text string and replaces:
        1) urls with URLHERE
        2) lots of whitespace with one instance
        3) mentions with MENTIONHERE

        This allows us to get standardized counts of urls and mentions
        Without caring about specific people mentioned
        """
        space_pattern = '\s+'
        giant_url_regex = (
            'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|'
            '(?:%[0-9a-fA-F][0-9a-fA-F]))+'
        )
        mention_regex = '@[\w\-]+'
        parsed_text = re.sub(space_pattern, ' ', text_string)
        parsed_text = re.sub(giant_url_regex, '', parsed_text)
        parsed_text = re.sub(mention_regex, '', parsed_text)

        return parsed_text

    def _tokenize(self, text):
        """
        - Remove pontuação e espaços a mais;
        - Coloca o texto em lowercase;
        - Stemiza os textos;
        :return lista dos tokens stemizados
        """

        lower_text = text.lower().strip()
        match_tokens = re.split('[^a-zA-Z]*', lower_text)
        new_text = ' '.join(match_tokens).strip()

        new_tokens = [self.stemmer.stem(tx) for tx in new_text.split()]

        return new_tokens

    def _basic_tokenize(self, text):
        """
        - Coloca o texto em lowercase;
        - Stemiza os textos;
        :return lista dos tokens
        """
        lower_text = text.lower().strip()
        match_tokens = re.split('[^a-zA-Z.,!?]*', lower_text)
        new_text = ' '.join(match_tokens).strip()
        new_tokens = new_text.split()

        return new_tokens

    def _count_twitter_objs(self, text_string):
        """
        Accepts a text string and replaces:
        1) urls with URLHERE
        2) lots of whitespace with one instance
        3) mentions with MENTIONHERE
        4) hashtags with HASHTAGHERE

        This allows us to get standardized counts of urls and mentions
        Without caring about specific people mentioned.

        Returns counts of urls, mentions, and hashtags.
        """
        space_pattern = '\s+'
        giant_url_regex = (
            'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|'
            '[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'
        )
        mention_regex = '@[\w\-]+'
        hashtag_regex = '#[\w\-]+'
        parsed_text = re.sub(space_pattern, ' ', text_string)
        parsed_text = re.sub(giant_url_regex, 'URLHERE', parsed_text)
        parsed_text = re.sub(mention_regex, 'MENTIONHERE', parsed_text)
        parsed_text = re.sub(hashtag_regex, 'HASHTAGHERE', parsed_text)
        return (
            parsed_text.count('URLHERE'),
            parsed_text.count('MENTIONHERE'),
            parsed_text.count('HASHTAGHERE')
        )